package facci.kleber.delgado.jugar;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class Perfil extends Activity {
    private EditText nombres, apellidos, edad, correo;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference usuario;
    private String userId;
    private FirebaseAuth firebaseAuth;
    private CircleImageView Fto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        setContentView(R.layout.activity_perfil);
        nombres = (EditText) findViewById(R.id.TXTNombresR);
        apellidos = (EditText) findViewById(R.id.TXTApellidosR);
        edad = (EditText) findViewById(R.id.TXTEdadR);
        correo = (EditText) findViewById(R.id.TXTCorreoR);
        Fto = (CircleImageView) findViewById(R.id.CirculoImagenRegistroR);


        /* Obtenemos instancia de la base de datos */
        firebaseDatabase = FirebaseDatabase.getInstance();

        /* Obtenemos instancia para autenticar el usuario*/
        firebaseAuth = FirebaseAuth.getInstance();

        /* Obtenemos instancia y referencias del almacenamiento de firebase */
        /* Obetenemos de la base de datos el usuario actual. */
        FirebaseUser user = firebaseAuth.getCurrentUser();
        userId = user.getUid();
        usuario = firebaseDatabase.getReference();
        Datos();

    }

    private void Datos() {

        usuario.child("Integrantes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (final DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Usuario usuario_perfil = snapshot.getValue(Usuario.class);
                    String key = usuario.child("Integrantes").child(userId).getKey();
                    String key2 = snapshot.getKey();
                    if (key.equals(key2)){
                        nombres.setText(usuario_perfil.getNombres());
                        apellidos.setText(usuario_perfil.getApellidos());
                        edad.setText(usuario_perfil.getEdad());
                        correo.setText(usuario_perfil.getCorreo());
                    }else {
                        Log.e("Nombre: ", usuario_perfil.getNombres());
                    }

                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

}




