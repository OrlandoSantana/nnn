package facci.kleber.delgado.jugar.modulo;

public class Persona {
    private String Uid;
    private String Nombre;

    public Persona(){

    }
    public String getUid() {
        return Uid;
    }

    public void setUid(String uid) {
        Uid = uid;
    }
    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }
    @Override
    public String toString() {
        return Nombre;
    }
}