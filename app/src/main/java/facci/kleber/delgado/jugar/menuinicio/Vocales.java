package facci.kleber.delgado.jugar.menuinicio;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.Toast;

import java.io.IOException;

import facci.kleber.delgado.jugar.R;

public class Vocales extends AppCompatActivity {

    Button BTNA, BTNE, BTNI, BTNO, BTNU;
    public MediaPlayer btna, btne, btni, btno, btnu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vocales);
        BTNA= (Button) findViewById(R.id.BTNA);
        BTNE= (Button) findViewById(R.id.BTNE);
        BTNI= (Button) findViewById(R.id.BTNI);
        BTNO= (Button) findViewById(R.id.BTNO);
        BTNU= (Button) findViewById(R.id.BTNU);

        btna = MediaPlayer.create(Vocales.this, R.raw.btna);
        btne = MediaPlayer.create(Vocales.this, R.raw.btne);
        btni = MediaPlayer.create(Vocales.this, R.raw.btni);
        btno = MediaPlayer.create(Vocales.this, R.raw.btno);
        btnu = MediaPlayer.create(Vocales.this, R.raw.btnu);



        //Funcion de boton para vocal A
        BTNA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btna.start();
                Toast.makeText(getApplicationContext(), "Vocal a", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal E
        BTNE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btne.start();
                Toast.makeText(getApplicationContext(), "Vocal e", Toast.LENGTH_SHORT).show();


            }
        });
        //Funcion de boton para vocal I
        BTNI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btni.start();
                Toast.makeText(getApplicationContext(), "Vocal i", Toast.LENGTH_SHORT).show();


            }
        });
        //Funcion de boton para vocal O
        BTNO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btno.start();
                Toast.makeText(getApplicationContext(), "Vocal o", Toast.LENGTH_SHORT).show();


            }
        });
        //Funcion de boton para vocal U
        BTNU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnu.start();
                Toast.makeText(getApplicationContext(), "Vocal u", Toast.LENGTH_SHORT).show();


            }
        });
    }
}
