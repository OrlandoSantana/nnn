package facci.kleber.delgado.jugar.menuinicio;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;

import facci.kleber.delgado.jugar.R;

public class Abecedario extends AppCompatActivity {


    Button BTNA, BTNB, BTNC, BTND, BTNE, BTNF, BTNG, BTNH, BTNI, BTNJ, BTNK, BTNL, BTNM, BTNN, BTNÑ, BTNO, BTNP, BTNQ, BTNR, BTNS, BTNT, BTNU, BTNV, BTNW, BTNX, BTNY, BTNZ;
    public  MediaPlayer btna, btnb, btnc, btnd, btne, btnf, btng,  btnh, btni, btnj, btnk, btnl,
    btnm, btnn, btnñ, btno, btnp, btnq, btnr, btns, btnt, btnu, btnv, btnw, btnx, btny, btnz;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abecedario);
        BTNA= (Button) findViewById(R.id.BTNA);
        BTNB= (Button) findViewById(R.id.BTNB);
        BTNC= (Button) findViewById(R.id.BTNC);
        BTND= (Button) findViewById(R.id.BTND);
        BTNE= (Button) findViewById(R.id.BTNE);
        BTNF= (Button) findViewById(R.id.BTNF);
        BTNG= (Button) findViewById(R.id.BTNG);
        BTNH= (Button) findViewById(R.id.BTNH);
        BTNI= (Button) findViewById(R.id.BTNI);
        BTNJ= (Button) findViewById(R.id.BTNJ);
        BTNK= (Button) findViewById(R.id.BTNK);
        BTNL= (Button) findViewById(R.id.BTNL);
        BTNM= (Button) findViewById(R.id.BTNM);
        BTNN= (Button) findViewById(R.id.BTNN);
        BTNÑ= (Button) findViewById(R.id.BTNÑ);
        BTNO= (Button) findViewById(R.id.BTNO);
        BTNP= (Button) findViewById(R.id.BTNP);
        BTNQ= (Button) findViewById(R.id.BTNQ);
        BTNR= (Button) findViewById(R.id.BTNR);
        BTNS= (Button) findViewById(R.id.BTNS);
        BTNT= (Button) findViewById(R.id.BTNT);
        BTNU= (Button) findViewById(R.id.BTNU);
        BTNV= (Button) findViewById(R.id.BTNV);
        BTNW= (Button) findViewById(R.id.BTNW);
        BTNX= (Button) findViewById(R.id.BTNX);
        BTNY= (Button) findViewById(R.id.BTNY);
        BTNZ= (Button) findViewById(R.id.BTNZ);

        // Crea la instancia para llamar al directory raw

        btna = MediaPlayer.create(Abecedario.this, R.raw.btna);
        btnb = MediaPlayer.create(Abecedario.this, R.raw.btnb);
        btnc = MediaPlayer.create(Abecedario.this, R.raw.btnc);
        btnd = MediaPlayer.create(Abecedario.this, R.raw.btnd);
        btne = MediaPlayer.create(Abecedario.this, R.raw.btne);
        btnf = MediaPlayer.create(Abecedario.this, R.raw.btnf);
        btng = MediaPlayer.create(Abecedario.this, R.raw.btng);
        btnh = MediaPlayer.create(Abecedario.this, R.raw.btnh);
        btni = MediaPlayer.create(Abecedario.this, R.raw.btni);
        btnj = MediaPlayer.create(Abecedario.this, R.raw.btnj);
        btnk = MediaPlayer.create(Abecedario.this, R.raw.btnk);
        btnl = MediaPlayer.create(Abecedario.this, R.raw.btnl);
        btnm = MediaPlayer.create(Abecedario.this, R.raw.btnm);
        btnn = MediaPlayer.create(Abecedario.this, R.raw.btnn);
        btnñ = MediaPlayer.create(Abecedario.this, R.raw.btnn2);
        btno = MediaPlayer.create(Abecedario.this, R.raw.btno);
        btnp = MediaPlayer.create(Abecedario.this, R.raw.btnp);
        btnq = MediaPlayer.create(Abecedario.this, R.raw.btnq);
        btnr = MediaPlayer.create(Abecedario.this, R.raw.btnr);
        btns = MediaPlayer.create(Abecedario.this, R.raw.btns);
        btnt = MediaPlayer.create(Abecedario.this, R.raw.btnt);
        btnu = MediaPlayer.create(Abecedario.this, R.raw.btnu);
        btnv = MediaPlayer.create(Abecedario.this, R.raw.btnv);
        btnw = MediaPlayer.create(Abecedario.this, R.raw.btnw);
        btnx = MediaPlayer.create(Abecedario.this, R.raw.btnx);
        btny = MediaPlayer.create(Abecedario.this, R.raw.btny);
        btnz = MediaPlayer.create(Abecedario.this, R.raw.btnz);

        //Funcion de boton para vocal A
        BTNA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btna.start();
                Toast.makeText(getApplicationContext(), "Letra A", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal B
        BTNB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnb.start();
                Toast.makeText(getApplicationContext(), "Letra B", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal C
        BTNC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnc.start();
                Toast.makeText(getApplicationContext(), "Letra C", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal D
        BTND.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnd.start();
                Toast.makeText(getApplicationContext(), "Letra D", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal E
        BTNE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btne.start();
                Toast.makeText(getApplicationContext(), "Letra E", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal F
        BTNF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnf.start();
                Toast.makeText(getApplicationContext(), "Letra F", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal G
        BTNG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btng.start();
                Toast.makeText(getApplicationContext(), "Letra G", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal H
        BTNH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnh.start();
                Toast.makeText(getApplicationContext(), "Letra H", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal I
        BTNI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btni.start();
                Toast.makeText(getApplicationContext(), "Letra I", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal J
        BTNJ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnj.start();
                Toast.makeText(getApplicationContext(), "Letra J", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal K
        BTNK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnk.start();
                Toast.makeText(getApplicationContext(), "Letra K", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal L
        BTNL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnl.start();
                Toast.makeText(getApplicationContext(), "Letra L", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal M
        BTNM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnm.start();
                Toast.makeText(getApplicationContext(), "Letra M", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal N
        BTNN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnn.start();
                Toast.makeText(getApplicationContext(), "Letra N", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal Ñ
        BTNÑ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnñ.start();
                Toast.makeText(getApplicationContext(), "Letra Ñ", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal O
        BTNO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btno.start();
                Toast.makeText(getApplicationContext(), "Letra O", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal P
        BTNP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnp.start();
                Toast.makeText(getApplicationContext(), "Letra P", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal Q
        BTNQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnq.start();
                Toast.makeText(getApplicationContext(), "Letra Q", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal R
        BTNR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnr.start();
                Toast.makeText(getApplicationContext(), "Letra R", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal S
        BTNS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btns.start();
                Toast.makeText(getApplicationContext(), "Letra S", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal T
        BTNT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnt.start();
                Toast.makeText(getApplicationContext(), "Letra T", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal U
        BTNU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnu.start();
                Toast.makeText(getApplicationContext(), "Letra U", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal V
        BTNV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnv.start();
                Toast.makeText(getApplicationContext(), "Letra V", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para vocal W
        BTNW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnw.start();
                Toast.makeText(getApplicationContext(), "Letra W", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para X
        BTNX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnx.start();
                Toast.makeText(getApplicationContext(), "Letra X", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para  Y
        BTNY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btny.start();
                Toast.makeText(getApplicationContext(), "Letra Y", Toast.LENGTH_SHORT).show();

            }
        });
        //Funcion de boton para Z
        BTNZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnz.start();
                Toast.makeText(getApplicationContext(), "Letra Z", Toast.LENGTH_SHORT).show();

            }
        });

    }
}
