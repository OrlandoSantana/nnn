package facci.kleber.delgado.jugar;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

import facci.kleber.delgado.jugar.menuinicio.Abecedario;
import facci.kleber.delgado.jugar.menuinicio.Mate;
import facci.kleber.delgado.jugar.menuinicio.Vocales;


public class Main2Activity extends AppCompatActivity {
    Button btn1,btn2,btn3;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    btn1=(Button)findViewById(R.id.btnumero);
    btn2=(Button)findViewById(R.id.btnabc);
    btn3=(Button)findViewById(R.id.btnvocal);
        firebaseAuth = FirebaseAuth.getInstance();
    btn1.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Main2Activity.this,Mate.class);
            startActivity(intent);
        }
    });

    btn2.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent=new Intent (Main2Activity.this,Abecedario.class);
            startActivity(intent);
        }
    });

    btn3.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent=new Intent (Main2Activity.this,Vocales.class);
            startActivity(intent);
        }
    });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return true;
    }

    // Agregamos las acciones cuando seleccionamos un item del menú.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.Cerrar_Sesion:
                firebaseAuth.signOut();
                startActivity(new Intent(Main2Activity.this, MainActivity.class));
                finish();
                break;

            case R.id.Perfil:
                startActivity(new Intent(Main2Activity.this, Perfil.class));
                break;
            case R.id.info:
                startActivity(new Intent(Main2Activity.this, Informacion.class));
                break;
            case R.id.Adivina:
                startActivity(new Intent(Main2Activity.this, Adivina.class));
                break;
            case R.id.Logros:
                startActivity(new Intent(Main2Activity.this, Logros.class));
                break;

        }
        return true;
    }


}
